#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "request.h"
#include "database.h"


//Finds node in database with node->name == name. If it doesn't exist, it returns NULL.
file_node *db_find(char *name) {
  file_node *current = database->head;
  while(current != NULL) {
    // If node has been found, return it.
    if (!strcmp(current->name, name)) {
      break;
    }
    current = current->next;
  }

  return current;
}

// Adds node to database.
void db_add(char *name, char *owner, bool shared) {
  file_node *fn = malloc(sizeof(file_node));
  fn->name = name;
  fn->owner = owner;
  fn->shared = shared;
  fn->prev = NULL;
  fn->next = NULL;
  if (database->head == NULL) {
    database->head = fn;
  } else {
    file_node *current = database->head;
    while (current->next != NULL) {
      current = current->next;
    }
    current->next = fn;
    fn->prev = current;
  }
}

// Deletes a node from database.
void db_del(file_node *node) {
  if (database->head == NULL) {
    printf("There are no files.");
  } else {
    if (node->prev == NULL){
      database->head = node->next;
    } else {
      node->prev->next = node->next;
    }
    free(node);
  }
} 

// Loads database from file.
// Format of the file: filename:owner:shared
void db_load(char* path) {
  database = malloc(sizeof(file_list));
  database->file_path = strdup(path);
  char *db_path = calloc(sizeof(path) + sizeof("/db") + 1, sizeof(char));
  strcat(db_path, path);
  strcat(db_path, "/db");
  database->head = NULL;
  char *name;
  char *owner;
  char *shared;
  // Read database.
  FILE* file;
  if (file = fopen(db_path, "a+")) {
    char entry[128];
    while (fgets(entry, sizeof(entry), file) != NULL) {
      name = strtok(entry,":");
      owner = strtok(NULL,":");
      shared = strtok(NULL,"\n");
      db_add(strdup(name), strdup(owner), strcmp(shared,"t") == 0);
    }
    fclose(file);
  }
}

// Save database in "{database->file_path}/db".
void db_save() {
  file_node *current = database->head;
  char *db_path = calloc(strlen(database->file_path) + 2 + 1, sizeof(char));
  strcat(db_path, database->file_path);
  strcat(db_path, "db");

  FILE* file = fopen(db_path, "w");
  if (file != NULL) {
    while (current != NULL) {
      //printf("%c\n",shared);
      // Write entry to file.
      fprintf (file, "%s:%s:%s\n", current->name, current->owner, current->shared ? "t":"f");
      current = current->next;
    }
    fclose(file);
    printf("Data saved.\n");
  }
}

// Converts the list of files to a string (used in LS request).
char *db_to_string() {
  int size = 1;
  file_node *current = database->head;
  if (current == NULL)
    return NULL;
  while (current != NULL) {
    size += strlen(current->name) + strlen(current->owner) + strlen("::t\n") + 1;
    current = current->next;
  }
  char *s = calloc(size, sizeof(char));
  current = database->head;
  while (current != NULL) {
    strcat(s,current->name);
    strcat(s,":");
    strcat(s,current->owner);
    char *shared = current->shared? "t":"f";
    strcat(s,":");
    strcat(s,shared);
    strcat(s,"\n");

    current = current->next;
  }
  return s;
}

// Prints the whole file list.
void db_print() {
  file_node *current = database->head;
  while (current != NULL) {
    printf("%s:%s:%d\n",current->name, current->owner, current->shared);
    current = current->next;
  }
}

// Frees list and nodes.
void db_free() {
  file_node *ptr = database->head;
  file_node *tmp = NULL;

  while (ptr != NULL) {
    tmp = ptr;
    ptr = ptr->next;
    free(tmp->name);
    free(tmp->owner);
    free(tmp);        
  }
  database->head = NULL;
  free(database);
}

// Creates file "file_name" and writes "contents" in it. File is created in database->filepath.
void create_file(char *file_name, char *contents) {
  char *path = calloc(strlen(database->file_path) + strlen(file_name) + 1, sizeof(char));
  strcat(path, database->file_path);

  strcat(path, file_name);

  FILE* file = fopen(path, "w");
  fputs(contents, file);
  fclose(file);
  db_add(strdup(file_name), current_user, false);
}

// Gets the whole file "file_name" (from database->file_path)
char *get_file(char *file_name) {
  char *path = calloc(strlen(database->file_path) + strlen(file_name) + 1, sizeof(char));
  strcat(path, database->file_path);
  strcat(path, file_name);

  FILE *f = fopen(path, "rb");
  fseek(f, 0, SEEK_END);
  long fsize = ftell(f);
  fseek(f, 0, SEEK_SET);

  char *content = calloc(fsize + 1, sizeof(char));
  fread(content, fsize, 1, f);
  fclose(f);

  content[fsize] = 0;
  free(path);
  return content;
}

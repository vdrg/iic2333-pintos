Para compilar: make all

*************************************

Servidor:
  El servidor constantemente intenta aceptar clientes. Cuando se acepta uno, lee desde este cliente y delega el mensaje recibido al método handle_request(char *request). handle_request decodifica el mensaje recibido y dependiendo del comando ejecuta el método respectivo (por ejemplo, si el mensaje recibido empieza con "PUT\n" entonces llama al método put_request con los parámetros extraídos del mensaje).
  handle_request retorna un string, que corresponde a la respuesta que luego se enviará al cliente. (Para más información, mirar request.h/.c)

  Para administrar los archivos, se implementó una base de datos simple que consiste en un archivo llamado "db" en el directorio de archivos del servidor. El archivo db contiene entradas de la forma: "nombre_archivo:dueño:shared", donde "shared" puede ser "t" o "f" dependiendo de si el archivo es compartido o no, respectivamente. Cada entrada en db se encuentra en una línea distinta (entradas separadas por \n).
  Cuando empieza el servidor, se lee el archivo db y se cargan las entradas en una lista doblemente ligada, llamada database. Si no hay entradas la lista comienza vacía, y si no existe el archivo db, entonces lo crea. (Para más información, mirar database.h/.c)

  Importante: sólo se pueden enviar archivos de menos de 1Mb de tamaño, esto ya que quisimos hacer el buffer de tamaño constante para enviar y recibir mensajes enteros (en el enunciado no se especifica nada de esto).

Cliente:


**************************************

Uso: Tal como aparece en el enunciado, es decir el usuario escribe COMMANDO, luego presiona ENTER, ingresa los parámetros. Al ingresar END y presionar ENTER el mensaje se envía completo al servidor.

#ifndef REQUEST_H 
#define REQUEST_H 


// User connected to server
bool client_connected;
char *current_user;
void read_from_client(int *c_socket, char *buffer);
void write_to_client(int *c_socket, char *msg, int len);
char *handle_request(char *request);
char *user_request(char *name);
char *put_request(char *file_name, char *content, int len);
char *get_request(char *file_name);
char *ls_request();
char *rm_request(char *file_name);
char *share_request(char *file_name);
char *close_request();
char *create_answer(char* parts[], int n);
#endif

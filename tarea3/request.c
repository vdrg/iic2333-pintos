#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <stdbool.h>
#include <fcntl.h>
#include <stdlib.h>
#include "request.h"
#include "database.h"

char resp_ok[] = "OK\nMessage: ";
char resp_fail[] = "FAIL\nMessage: ";
char resp_end[] = "END\n";

char *current_user = NULL;
char *handle_request(char *req) {
  char *ans;
  char *r = strdup(req);
  // Get command from request.
  char *command = strtok(r, "\n");

  if (strcmp(command, "USER") == 0) { //USER request
    char *param = strtok(NULL, " ");
    char *username = strtok(NULL, "\n");

    ans = user_request(username);
  } else {
    if (current_user == NULL) {
      ans = "FAIL. You need to login first.";
    } else if (strcmp(command, "PUT") == 0) { //PUT request
      char *param = strtok(NULL, " ");
      char *file_name = strtok(NULL, "\n");
      param = strtok(NULL, " ");
      int length = atoi(strtok(NULL, "\n"));
      char *content = calloc(1024*1024, sizeof(char));
      strncpy(content, strtok(NULL, ""), length);

      ans = put_request(file_name, content, length);
      free(content);
      db_save();
    } else if (strcmp(command, "GET") == 0) { //GET request
      char *param = strtok(NULL, " ");
      char *file_name = strtok(NULL, "\n");

      ans = get_request(file_name);
    } else if (strcmp(command, "LS") == 0) { //LS request
      ans = ls_request();
    } else if (strcmp(command, "RM") == 0) { //RM request
      char *param = strtok(NULL, " ");
      char *file_name = strtok(NULL, "\n");

      ans = rm_request(file_name);
      db_save();
    } else if (strcmp(command, "SHARE") == 0) { //SHARE request
      char *param = strtok(NULL, " ");
      char *file_name = strtok(NULL, "\n");

      ans = share_request(file_name);
      db_save();
    } else if (strcmp(command, "CLOSE") == 0) { //CLOSE request
      ans = close_request();
    }
  }
  free(r);
  return strdup(ans);
}

void read_from_client(int *c_socket, char *buffer) {
  // Fill buffer with 0s.
  bzero(buffer,1024*1024);

  // Read from client.
  if (read(*c_socket,buffer,1024*1024 - 1) < 0) 
    perror("ERROR reading from socket");
}

void write_to_client(int *c_socket, char *msg, int len) {
  // Send to client.
	if (write(*c_socket, msg,len) < 0) 
		perror("ERROR writing to socket");
}

char *user_request(char *name) {
  current_user = strdup(name);

  char *msg = "User identified as ";
  char *parts[] = {resp_ok, msg, current_user, "\n", resp_end};
  return create_answer(parts, 5);
}

char *put_request(char *file_name, char *content, int len) {
  char *msg;
  char *ans;
  file_node *file = db_find(file_name); // Check if file already exists.
  if (file != NULL) {
    msg = "A file named like that already exists.";
    char *parts[] = {resp_fail, msg, "\n", resp_end};
    ans = create_answer(parts, 4);

  } else if (len > 1024*1024) {
    msg = "Maximum file size is 1Mb.";
    char *parts[] = {resp_fail, msg, "\n", resp_end};
    ans = create_answer(parts, 4);
  } else {
    create_file(file_name, content);
    msg = "File uploaded.";
    char *parts[] = {resp_ok, msg, "\n", resp_end};
    ans = create_answer(parts, 4);
  }
  return ans;
}

char *get_request(char *file_name) {
  file_node *file = db_find(file_name);
  char *msg;

  if (file == NULL) {
    msg = "File not found.";
    char *parts[] = {resp_fail, msg, "\n", resp_end};
    return create_answer(parts, 4);
  } else if (strcmp(current_user,file->owner) && !file->shared) {
    msg = "You are not allowed to do this.";
    char *parts[] = {resp_fail, msg, "\n", resp_end};
    return create_answer(parts, 4);
  } else {
    msg = "Getting file from database.";
    char *contents = get_file(file_name);
    char *parts[] = {resp_ok, msg, "\n", contents,"\n", resp_end};
    
    return create_answer(parts, 6); 
  }
  return NULL;
}

char *ls_request() {
  char *msg = "Listing files in server.";
  char *ls = db_to_string();
  char *parts[] = {resp_ok, msg, "\n",ls, resp_end};
  return create_answer(parts, 5); 
}

char *rm_request(char *file_name) {
  file_node *file = db_find(file_name);
  char *msg;

  if (file == NULL) {
    msg = "File not found.";
    char *parts[] = {resp_fail, msg, "\n", resp_end};
    return create_answer(parts, 4);
  } else if (strcmp(current_user,file->owner) != 0) {
    msg = "You are not allowed to do this.";
    char *parts[] = {resp_fail, msg, "\n", resp_end};
    return create_answer(parts, 4);
  } else {
    char *path = calloc(strlen(database->file_path) + strlen(file_name) + strlen("/") + 1, sizeof(char));
    strcat(path, database->file_path);
    strcat(path, "/");
    strcat(path, file_name);

    db_del(file);
    remove(path);
    msg = "Your file has been deleted.";
    char *parts[] = {resp_ok, msg, "\n", resp_end};
    return create_answer(parts, 4);
  }
}

char *share_request(char *file_name) {
  file_node *file = db_find(file_name);
  char *msg;
  if (file == NULL) {
    msg = "File not found.";
    char *parts[] = {resp_fail, msg, "\n", resp_end};
    return create_answer(parts, 4);
  } else if (strcmp(current_user,file->owner)) {
    msg = "You are not the owner of this file.";
    char *parts[] = {resp_fail, msg, "\n", resp_end};
    return create_answer(parts, 4);
  } else {
    file->shared = !file->shared;
    if (file->shared) {
      msg = "Your file is now shared!";
    } else {
      msg = "Your file is now private.";
    }
    char *parts[] = {resp_ok, msg, "\n", resp_end};
    return create_answer(parts, 4);
  }
}

char *close_request() {
  free(current_user);
  current_user = NULL;
  client_connected = false;
  char *msg = "Connection closed.";
  char *parts[] = {resp_ok, msg, "\n", resp_end};
  return create_answer(parts, 4);
}

// Builds string from 'parts'. n = size of parts.
char *create_answer(char *parts[], int n){
  char *ans;
  int size = 1;
  int i;
  for (i = 0; i < n; i++) {
    size += strlen(parts[i]);
  }
  ans = calloc(size, sizeof(char));
  for (i = 0; i < n; i++) {
    strcat(ans,parts[i]);
  }
  return ans;
}

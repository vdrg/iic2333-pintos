#ifndef USERPROG_SYSCALL_H
#define USERPROG_SYSCALL_H

void syscall_init (void);
void halt(void);
void salir(int x);
int ejecutar(const char *param);

#endif /* userprog/syscall.h */

#ifndef DATABASE_H 
#define DATABASE_H 

#include <stdbool.h>

struct file_node {
  char *name;
  char *owner;
  bool shared;
  struct file_node *prev;
  struct file_node *next;
};
typedef struct file_node file_node;

struct file_list {
  char *file_path;
  file_node *head;
};
typedef struct file_list file_list;

/************************/
file_list *database;
//char file_path[64];
/************************/

file_node *db_find(char *name);
void db_add(char *name, char *owner, bool shared);
void db_del(file_node *node); 
void db_load(char* path);
void db_save();
char *db_to_string();
void db_print();
void db_free();
char *read_file(char *file_name);
void create_file(char *file_name, char *contents);
char *get_file(char *file_name);
#endif

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>

bool client_running = false;
void error(const char *msg);
void get_input(char *buffer);

int main(int argc, char *argv[])
{
  int sockfd;
  int port_number;
  int n;
  struct sockaddr_in serv_addr;
  struct hostent *server;

  char *buffer = calloc(1024*1024, sizeof(char));
  if (argc < 3) {
    fprintf(stderr,"Usage %s hostname port\n", argv[0]);
    exit(0);
  }

  port_number = atoi(argv[2]);

  sockfd = socket(AF_INET, SOCK_STREAM, 0);
  if (sockfd < 0)
    error("ERROR opening socket");

  server = gethostbyname(argv[1]);
  if (server == NULL) {
    fprintf(stderr,"ERROR, no such host\n");
    exit(0);
  }

  // Setup connection to server.
  bzero((char *) &serv_addr, sizeof(serv_addr));
  serv_addr.sin_family = AF_INET;
  bcopy((char *)server->h_addr,
      (char *)&serv_addr.sin_addr.s_addr,
      server->h_length);
  serv_addr.sin_port = htons(port_number);

  if (connect(sockfd,(struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0)
    error("ERROR connecting");

  // Main loop.
  client_running = true;
  while (client_running) {

    get_input(buffer);

    if (write(sockfd, buffer, strlen(buffer)) < 0) {
      error("Error sending request");
    }
    bzero(buffer, 1024*1024);
    if (read(sockfd, buffer, 1024*1024-1) < 0) {
      error("Error reading from server");
    }
    
    printf("%s\n\n", buffer);

  }

  close(sockfd);
  return 0;
}

void get_input(char *buffer) {
  int max_len = 1024*1024;
  bzero(buffer, max_len);
  char *line = calloc(max_len, sizeof(char)); 
  printf("Please enter a request.\n");
  bool first_line = true;
  do {
    if (fgets(line, max_len, stdin ) == NULL)
      break;
    if (strcmp(line,"CLOSE\n") == 0 && first_line) {
      client_running = false;
    }

    first_line = false;
    strcat(buffer, line);
  } while (strcmp(line,"END\n"));
}

void error(const char *msg)
{
  perror(msg);
  exit(0);
}

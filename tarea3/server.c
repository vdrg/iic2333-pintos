#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <signal.h>
#include "database.h"
#include "request.h"


void config_server(int *port);
void init_server_socket(int *s_socket, int port);
void accept_client(int *s_socket, int *c_socket);
void error(const char *msg);

int main() {

  // Definitions
  bool server_running = true;
  client_connected = false;
  int port;
	int s_socket, c_socket;
	socklen_t c_len; // Client length.
	struct sockaddr_in client_addr;
	int n;
  char *buffer = calloc(1024 * 1024, sizeof(char) );

  // Read fileserver.conf and load database.
  config_server(&port);
  init_server_socket(&s_socket, port);

  // TESTING
  //printf("%s",db_to_string());
  /*
  char *req0 = "USER\nName: Vicente\nEND\n";
  char *req1 = "USER\nName: Choromota\nEND\n";
  char *req2 = "PUT\nName: hola.txt\nLength: 10\nabcdeabcde\nEND\n";
  char *req3 = "LS\nEND\n";
  char *req4 = "GET\nName: asd.txt\nEND\n";
  char *req5 = "SHARE\nName: hola.txt\nEND\n";
  char *req6 = "RM\nName: hola.txt\nEND\n";
  
  char *ans = handle_request(req0);
  printf("%s\n", ans);
  ans = handle_request(req2);
  printf("%s\n", ans);*/

	// Listen for clients.
	listen(s_socket,5);
	printf("Server listening...\n");	

  // Main loop.
  while (server_running) {
    accept_client(&s_socket, &c_socket);
    if (c_socket < 0) {
      perror("Accept stopped");
      printf("Closing server...\n");
      server_running = false;
      continue;
    }

    printf("Client accepted.\n");

    // Communication between client and server.
    client_connected = true;
    while (client_connected) {
      read_from_client(&c_socket, buffer);
      char *ans = handle_request(buffer);
      printf("%s\n", ans);
      write_to_client(&c_socket, ans, strlen(ans));
      free(ans);
    }
  }
  // Close server
  printf("Freeing buffer...\n");
  free(buffer);
  printf("Closing server socket...\n");
  close(s_socket);
  printf("Freeing database...\n");
  db_free();
  return 0;
}


// Read fileserver.conf and load database.
void config_server(int *port) {
  // Server Configurations
  FILE* file = fopen("./fileserver.conf", "r");
  char file_path[64];
  if (fscanf(file,"%u\n%s",port,file_path) == EOF){
    printf("Error reading config file.\n");
  } else {
    printf("Port set to: %u\nFiles are located in %s\n", *port, file_path);
  }
  fclose(file);

  strcat(file_path, "/");
  // Load database from file (if it exists).
  db_load(file_path);
}

void init_server_socket(int *s_socket, int port) {
  struct sockaddr_in s_addr;
  *s_socket = socket(AF_INET, SOCK_STREAM, 0);
	
	if (s_socket < 0) 
		error("ERROR opening socket");

	bzero((char *) &s_addr, sizeof(s_addr));
	s_addr.sin_family = AF_INET;
	s_addr.sin_addr.s_addr = INADDR_ANY;
	s_addr.sin_port = htons(port);

	if (bind(*s_socket, (struct sockaddr *) &s_addr, sizeof(s_addr)) < 0) 
			 error("ERROR on binding");
}

void accept_client(int *s_socket, int *c_socket) {
  struct sockaddr_in c_addr;
  socklen_t c_len = sizeof(c_addr);
  *c_socket = accept(*s_socket, 
      (struct sockaddr *) &c_addr, 
      &c_len);
}

void error(const char *msg)
{
	perror(msg);
	exit(1);
}
